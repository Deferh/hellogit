# include <iostream>
# include <string>
# include <vector>

using namespace std;

int main()
{
		auto check = "GO";
		string a = "GO";
		vector<int> v = {10, 22};
		
		if(a == check)
		{
			v.push_back(14);
			v.push_back(32);
			v.push_back(64);
			v.push_back(16);
		}
			
		//Shows vector and its values at the start
		for(unsigned int i = 0; i < v.size(); i++)
		{	
			cout << v[i] << " ";
		}
		
		cout << endl;
		
		//pop_back 1 and adds 1 to the end of the vector
		for(unsigned int i = 0; i < v.size(); i++)
		{	
			cout << v[i] << " ";
			
			v.pop_back();
			v.push_back(1);
		}
		
		cout << endl;
		
		//pop_back 2 and adds 2 to the end of the vector
		for(unsigned int i = 0; i < v.size(); i++)
		{	
			cout << v[i] << " ";
			
			while( v.size() != 4)// while the size of vector v is not equal to 4 pop_back
			{
				v.pop_back();
			}
			v.push_back(2);
		}
		
		for(unsigned int i = 5; i < v.size(); i++)
		{	
			cout << v[i] << " ";
		}
		return 0;
}
